def d2mn(m,n):
    M=matrix(QQ,2^n,2^n)
    for i in range(2^(m-1)):
        M[i,2*i]=1/2
        M[i,2*i+1]=1/2
        M[2^(m-1)+i,2*i]=1/2
        M[2^(m-1)+i,2*i+1]=-1/2
    for i in range(2^m,2^n):
        M[i,i]=1
    return(M)


def d2(n):
    A = d2mn(n,n)
    for i in range(n-1):
        A = d2mn(n-i,n)*A
    return (A)

D2=d2(4)
D2i=D2.inverse()

v = [128+randint(-3,3) for i in range(16)]

vm=matrix(QQ,16,1,v)
v1 =D2*vm

v2=matrix(QQ,16,1)

for i in range(16):
    if abs(v1[i])<1:
        v2[i]= 0
    else:
        v2[i]=v1[i]

v3 = D2i*v2

vm, v3

